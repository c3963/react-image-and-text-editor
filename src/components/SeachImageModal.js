import React, { useEffect, useState } from 'react';
import { v4 } from 'uuid';
import { seachImage } from '../API/Unsplash';

const SearchImageModal = ({ show, setBG }) => {
    // STATES
    const [searchValue, setSearchValue] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const [searchResultItems, setSearchResultItems] = useState([]);
    const [selectedImage, setSelectedImage] = useState(null);
    const [loading, setLoading] = useState(false);

    // EVENTS

    useEffect(() => {
        // console.log(show);
    }, [show]);

    // CUSTOM EVENT HANDLERS
    const searchImageAction = () => {
        setLoading(true);
        seachImage(searchValue)
            .then((res) => {
                setSearchResults(res);
                setLoading(false);
            })
            .catch(() => {
                setLoading(false);
            });
    };

    useEffect(() => {
        if (searchResults && searchResults.data && searchResults.data.results) {
            setSearchResultItems(searchResults.data.results);
        }
    }, [searchResults]);

    return (
        <div
            className={`modal fade ${show ? 'show' : ''} bd-example-modal-lg`}
            id="exampleModal"
            tabIndex="-1"
            role="dialog"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
        >
            <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">
                            Search Image (Unsplash.com)
                        </h5>
                        <button
                            type="button"
                            className="close"
                            data-dismiss="modal"
                            aria-label="Close"
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <div className="row">
                            <div className="col-12 text-left">
                                <div className="form-group">
                                    <label htmlFor="search">Search</label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        onChange={(e) => {
                                            setSearchValue(e.target.value);
                                        }}
                                    />
                                </div>
                                <button
                                    className="btn btn-primary btn-sm"
                                    onClick={() => {
                                        searchImageAction();
                                    }}
                                >
                                    Search
                                </button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="container">
                                {!loading ? (
                                    <div className="row">
                                        {searchResultItems ? (
                                            searchResultItems.map((item) => {
                                                return (
                                                    <div
                                                        className={`col-4 search-item ${
                                                            selectedImage &&
                                                            item.id ===
                                                                selectedImage.id
                                                                ? 'selected-item'
                                                                : ''
                                                        }`}
                                                        key={item.id}
                                                        onClick={() => {
                                                            setSelectedImage(
                                                                item
                                                            );
                                                        }}
                                                    >
                                                        <img
                                                            className="image-item"
                                                            src={
                                                                item.urls.thumb
                                                            }
                                                            alt={
                                                                item.alt_description
                                                            }
                                                        />
                                                        <label>
                                                            Photo by:{' '}
                                                            {item.user.username}
                                                        </label>
                                                    </div>
                                                );
                                            })
                                        ) : (
                                            <div className="col-12">
                                                <label>No Results</label>
                                            </div>
                                        )}
                                    </div>
                                ) : (
                                    <div className="row">
                                        <div className="col-12">
                                            <div
                                                className="spinner-border"
                                                role="status"
                                            >
                                                <span className="">
                                                    Loading...
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button
                            type="button"
                            className="btn btn-secondary"
                            data-dismiss="modal"
                        >
                            Close
                        </button>
                        <button
                            disabled={selectedImage != null ? false : true}
                            type="button"
                            className="btn btn-primary"
                            onClick={() => {
                                setBG(selectedImage.urls.regular);
                            }}
                            data-dismiss="modal"
                        >
                            Set Background
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SearchImageModal;
