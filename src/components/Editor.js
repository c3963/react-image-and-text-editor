import React, { useState, useEffect } from 'react';
import '../styles/editor.css';
import SearchImageModal from './SeachImageModal';
import * as htmlToImage from 'html-to-image';

const Editor = () => {
    const [bgUrl, setBgUrl] = useState(
        'https://images.unsplash.com/photo-1637498127152-3f8296b97dfd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80'
    );

    const [canEditText, setCanEditText] = useState(false);
    const [showSearchModal, setShowSearchModal] = useState(false);

    useEffect(() => {
        // console.log(canEditText);
    }, []);

    // EVENTS
    const toggleTextEdit = () => {
        setCanEditText(!canEditText);
    };

    const toggleSearch = () => {
        setShowSearchModal(!showSearchModal);
    };

    return (
        <div className="card mt-4">
            <div className="card-header text-right">
                <button
                    className="btn download-button"
                    onClick={() => {
                        htmlToImage
                            .toJpeg(
                                document.getElementById('image-container'),
                                {
                                    quality: 0.95,
                                }
                            )
                            .then(function (dataUrl) {
                                var link = document.createElement('a');
                                link.download = 'Downloaded-Image';
                                link.href = dataUrl;
                                link.click();
                            });
                    }}
                >
                    Download
                </button>
            </div>
            <div className="card-body">
                <div className="row">
                    <div className="col-md-2 text-center option-container">
                        <button
                            className="custom-button button-spacing"
                            onClick={() => {
                                toggleTextEdit();
                            }}
                        >
                            <i
                                className="fa fa-font font-icon"
                                aria-hidden="true"
                            ></i>
                            <br />
                            Text
                        </button>
                        <button
                            className="custom-button button-spacing"
                            data-toggle="modal"
                            data-target="#exampleModal"
                            onClick={() => {
                                toggleSearch();
                            }}
                        >
                            <i
                                className="fa fa-file-image-o font-icon"
                                aria-hidden="true"
                            ></i>
                            <br />
                            Background
                        </button>
                    </div>
                    <div
                        className="col-md-10 bg-style"
                        id="image-container"
                        style={{ backgroundImage: `url(${bgUrl})` }}
                    >
                        <textarea
                            type="text"
                            className="custom-text-field"
                            disabled={!canEditText}
                            defaultValue="Sample Text"
                        ></textarea>
                    </div>
                </div>
            </div>
            <SearchImageModal show={showSearchModal} setBG={setBgUrl} />
        </div>
    );
};

export default Editor;
