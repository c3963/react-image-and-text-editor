// import logo from './logo.svg';
import './App.css';
import Editor from './components/Editor';

function App() {
    return (
        <div className="App">
            <div className="row">
                <div className="offset-md-2 col-md-8">
                    <Editor />
                </div>
            </div>
        </div>
    );
}

export default App;
