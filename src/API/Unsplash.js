import axios from 'axios';

const baseUrl = 'https://api.unsplash.com';

export const seachImage = async (searchKey) => {
    const url = `${baseUrl}/search/photos?query=${searchKey}`;
    const result = await axios.get(url, {
        headers: {
            Authorization:
                'Client-ID gP6CCkfSGPlugZ5XevySbnrLaGUmLF49bLBCpkgJg4Y',
        },
    });
    return result;
};
